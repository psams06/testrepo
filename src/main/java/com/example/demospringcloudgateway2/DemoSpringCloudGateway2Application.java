package com.example.demospringcloudgateway2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringCloudGateway2Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringCloudGateway2Application.class, args);
	}

}
